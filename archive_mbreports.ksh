#! /usr/bin/ksh
########################################################################################################
# This script moves print logs from a micro lab process to the BCH sftp server.
# Unlike archive_printlogs.ksh, this job runs after the print process, so it is guarabnteed to be in the
# SAME node as the orint process.  Thsi simplifies th elogic, since we do nto have to copy files from
# one node to another, and can copy files directly to teh print server.
#
# Shamelessly copied code chunks from the work by Curtis and Vijai on dscrnftp_runner.ksh
# Author: Jowell Sabino
# Date: 09/28/2020
########################################################################################################

### SETUP VARS ###
##################
SOURCE_DIR=${cer_print}

SSH_SERVER="cisftpprd.tch.harvard.edu"
#SSH_SERVER="cisftpdevndc1.tch.harvard.edu"
SSH_USER="dscrnftp"
#SSH_USER="cxpftp"

log_file=$cust_reports/log/dscrnftp_printlogs.log

YESTERDAY=$(date -d "1 day ago" '+%m%d')
TODAY=$(date '+%m_%d_%Y_%H_%M_%S')
TODAYSHORT=$(date '+%m%d%H')
PROG=$0
cerner_user="d_${environment}"

EXTENSION="txt"

##########
## MAIN ##
##########

# In KSH, use "set -A" instead of "declare -a" that is used in bash
# Only look for file ouput for last 60 minutes
set -A source_files $(find ${SOURCE_DIR}/mic_{far,car}*.txt -mmin -10) 2>> ${log_file}

# Keep last 500 lines of $log_file
TMP=$(tail -n 500 $log_file 2>/dev/null) && echo "${TMP}" > ${log_file}
echo >> ${log_file}


echo "${TODAY} - Running ${PROG}..." >> ${log_file}
echo >> ${log_file}

errcopy="0"
errsftp="0"
{


   for file in "${source_files[@]}";
   do
   
        # Start Report Viewer
       DESTINATION=$(awk '/Sequence:/ { print tolower($2) tolower($3) tolower($4)}' $file)
       SSH_DEST_DIR="/RVOutgoing"

       echo "Transfer: ${file} to ${SSH_USER}@${SSH_SERVER}:${SSH_DEST_DIR}/${DESTINATION}.${EXTENSION}" >> ${log_file}
       echo >> ${log_file}

       /usr/bin/scp -p ${file} ${SSH_USER}@${SSH_SERVER}:${SSH_DEST_DIR}/${DESTINATION}.${EXTENSION}
       errsftp=$?

       if [[ ${errsftp} -eq "0" ]]
       then

           echo "File ${file} successfully SCP-ed to server ${SSH_SERVER} ${SSH_DEST_DIR}/${DESTINATION}.${EXTENSION}." >> ${log_file}

       else

           echo "Error connecting to: ${SSH_SERVER}. ${file} not transferred." >> ${log_file}
       fi
       echo >> ${log_file}
       # End Report Viewer
	
	   # Start ROME
       DESTINATION=$(awk '/Sequence:/ { print tolower($2) "_" tolower($3) "_"}' $file)$TODAY
       SSH_DEST_DIR='/rome_reports/extract/labs/processing'

       echo "Transfer: ${file} to ${SSH_USER}@${SSH_SERVER}:${SSH_DEST_DIR}/${DESTINATION}.${EXTENSION}" >> ${log_file}
       echo >> ${log_file}

       /usr/bin/scp -p ${file} ${SSH_USER}@${SSH_SERVER}:${SSH_DEST_DIR}/${DESTINATION}.${EXTENSION}
       errsftp=$?

       if [[ ${errsftp} -eq "0" ]]
       then

           echo "File ${file} successfully SCP-ed to server ${SSH_SERVER} ${SSH_DEST_DIR}/${DESTINATION}.${EXTENSION}." >> ${log_file}

       else

           echo "Error connecting to: ${SSH_SERVER}. ${file} not transferred." >> ${log_file}
       fi
       echo >> ${log_file}
       # End ROME


   done


} 2>>${log_file}

exit $((errcopy || errsftp))
